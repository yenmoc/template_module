# __REPOSITORY_NAME__

## What

* 

## Requirement

* 

## Install

```shell
yarn add "git+ssh://git@gitlab.com:__REPOSITORY_GROUP__/__REPOSITORY_NAME__"
```

## Usage

* 

## License

Copyright (c) __COPYRIGHT_YEAR__ __COPYRIGHT_NAME__

Released under the __LICENSE__ license, see [LICENSE.txt](LICENSE.txt)

